﻿var app = angular.module("FestiveMall", ["festiveMallServices", "ui.bootstrap", "ui.router", "ngFileUpload", "ngAlertify", "bootstrapLightbox"]);




app.config(["$stateProvider", "$urlRouterProvider", "$provide", "$httpProvider", function ($stateProvider, $urlRouterProvider, $provide, $httpProvider) {
    $provide.decorator("$exceptionHandler", function ($delegate) {
        return function (exception, cause) {
            $delegate(exception, cause);
            //alert(exception);
        };
    });
    //$httpProvider.interceptors.push("httpInterceptor");
    $urlRouterProvider.otherwise("/Home");
    $stateProvider
        .state("Home", {
            url: "/Home",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/home.html",
                    controller: "HomeCtrl"

                }
            }
        })
        .state("AboutUs", {
            url: "/AboutUs",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/aboutus.html",
                    controller: "AboutUsCtrl"

                }
            }
        })
        .state("ContactUs", {
            url: "/ContactUs",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/contactus.html",
                    controller: "ContactUsCtrl"

                }
            }
        })
        .state("Shop", {
            url: "/Shop",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/shop.html",
                    controller: "ShopCtrl"

                }
            }
        })
        .state("SingleShop", {
            url: "/SingleShop",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/single-shop.html",
                    controller: "SingleCtrl"
                }
            }
        })
        .state("Admin", {
            url: "/Admin",
            views: {
                "htmlbody@": {
                    templateUrl: "templates/admin.html",
                    controller: "AdminCtrl"

                }
            }
        });
}]);
app.factory('httpInterceptor', ["$q", "$rootScope", function ($q, $rootScope) {
    var numLoadings = 0;
    return {
        request: function (config) {
            if (config.data !== undefined) {
                if (config.data.loaderStatus === undefined) {
                    numLoadings++;
                    $rootScope.$broadcast("loader_show");
                    return config || $q.when(config);
                } else {
                    numLoadings++;
                    return config || $q.when(config);
                }
            } else {
                numLoadings++;
                $rootScope.$broadcast("loader_show");
                return config || $q.when(config);
            }

        },
        response: function (response) {
            if ((--numLoadings) === 0) {
                $rootScope.$broadcast("loader_hide");
            }
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (!(--numLoadings)) {
                $rootScope.$broadcast("loader_hide");
            }
            return $q.reject(response);
        }
    };
}]);
app.directive("loader", [function () {
    return function ($scope, element, attrs) {
        $scope.$on("loader_show", function () {
            return element.show();
        });
        return $scope.$on("loader_hide", function () {
            return element.hide();
        });
    };
}]);