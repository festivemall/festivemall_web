﻿app.controller("ShopCtrl", ["$rootScope", "$scope", "festiveMallMasterService", function ($rootScope, $scope, festiveMallMasterService) {
    $scope.Categories = [];


    $scope.GetCategories = function () {
        festiveMallMasterService.GetCategories(onsuccess, onerror);
        function onsuccess(resp) {
            $scope.Categories = resp.data;


            angular.forEach($scope.Categories, function (item) {
                item.IsActive = false;
            });
            var obj = {
                "CategoryName": "All",
                "CategoryId": null,
                "IsActive": false
            };
            $scope.Categories.unshift(obj);

            angular.forEach($scope.Categories, function (item) {
                if ($rootScope.homeCategoryId === item.CategoryId) {
                    item.IsActive = true;
                }
            });

        }
        function onerror(error) {
            console.log(error);
            $scope.Categories = [];
            alert("Getting Categories Failed");
        }
    };


    $scope.ChangeCatgorie = function (cat) {
        angular.forEach($scope.Categories, function (item) {
            item.IsActive = false;
        });
        cat.IsActive = true;
        $scope.VarientSearch.CategoryId = cat.CategoryId;
        $scope.VarientSearch.CategoryName = cat.CategoryName;
        $scope.GetVarientByProductId();
    };



     
    $scope.StockList = [];
    $scope.StockCount = 0;
    $scope.changeStockPage = function () {

        $scope.GetVarientByProductId();

    };

    $scope.VarientSearch = {
        "Searchtext": null,
        "PageSize": 20,
        "PageNo": 1,
        "CategoryId": $rootScope.homeCategoryId,
        "CategoryName": $rootScope.homeCategoryName
    };
    $scope.GetVarientByProductId = function () {
        festiveMallMasterService.GetVarientByProductId($scope.VarientSearch).then(function (resp) {
            if (resp.data.Status !== 1) {
                $scope.StockList = [];
                $scope.StockCount = 0;
                alert(resp.data.Message);
                return;
            } else {
                $scope.StockList = resp.data.data.rows;
                $scope.StockCount = resp.data.data.count;
            }
        }, function (err) {
            console.log(err);
            $scope.StockList = [];
            $scope.StockCount = 0;
            alert("Getting StockList Failed");
        });
    };
    $scope.Init = function () {
        $scope.GetCategories();

        $scope.GetVarientByProductId();
    };
    $scope.Init();
}]);