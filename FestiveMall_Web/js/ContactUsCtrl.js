﻿app.controller("ContactUsCtrl", ["$scope", "festiveMallMasterService", "alertify", function ($scope, festiveMallMasterService, alertify) {
    $scope.Enquiry = {
        "Name": null,
        "Email": null,
        "Mobile": null,
        "Reason": null,
        "Comment": null,

        "form": ""
    };
    $scope.SendEnquiry = function (form) {
        festiveMallMasterService.SendMailToAdmin($scope.Enquiry).then(function (resp) {
            if (resp.data.Status !== 1) {
                alertify.error(resp.data.Message);
                return;
            } else {
                alertify.success("Enquiry Sent");
                form.$setPristine();
                $scope.Enquiry = {
                    "Name": null,
                    "Email": null,
                    "Mobile": null,
                    "Reason": null,
                    "Comment": null,

                    "form": ""
                };
            }
        }, function (err) {
            console.log(err);
            alertify.error("Enquiry Sending Failed");
        });
    };
}]);