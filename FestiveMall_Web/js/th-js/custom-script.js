jQuery(document).ready(function ($) {
 "use strict";

//
//----------------------------------


//wow js active
//----------------------------------
 new WOW().init();


//js scrollup
//----------------------------------
$.scrollUp({
	scrollText: '<i class="fa fa-angle-double-up"></i> TOP',
	easingType: 'linear',
	scrollSpeed: 900,
	animation: 'fade'
});
// Data images
//----------------------------------
$('.background-image').each(function () {
	var src = $(this).attr('data-src');
	$(this).css({
		'background-image': 'url(' + src + ')'
	});
});

//google map activation
//-----------------------------------
	if ($('#gmap').length > 0) {
		new GMaps({
			div: '#gmap',
			lat: 31.6175419, // //31.6175419,74.2811501
			lng:74.2811501,
			scrollwheel: false,
			styles: [
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dddddd"
						},
						{
							"lightness": 20
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 17
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 29
						},
						{
							"weight": 0.2
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 18
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dddddd"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						},
						{
							"lightness": 21
						}
					]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#ffffff"
						},
						{
							"lightness": 16
						}
					]
				},
				{
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				}
			]
		}).addMarker({
			lat: 23.792930, //23.792930, 90.403798
			lng: 90.403798,
			infoWindow: {
				content: '<p>Innwit Themes</p>'
			}
			});

	}
	//slider jquery

	$(".slider-one-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});

	$(".slider-three-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});

	$(".home-two-slider").owlCarousel({
		animateOut: 'slideOutDown',
		animateIn: 'flipInX',
		transitionStyle : "fade",
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :true,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-four-area").owlCarousel({
		animateOut: 'slideOutDown',
		animateIn: 'flipInX',
		transitionStyle : "fade",
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-five-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		//paginationNumbers: true,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-seven-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-eight-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :false,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-nine-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :false,
		pagination :true,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	$(".slider-ten-area").owlCarousel({
		items:1,
		autoPlay : true,
		navigation :true,
		navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		pagination :true,
		slideSpeed : 200,
		responsive: true,
		autoHeight : true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [980,1],
		itemsTablet: [768,1],
		itemsMobile : [479,1],
	});
	//search box focus effect
	//----------------------------------
	$('#search-form .form-control').on('focusin', function () {
		$('#search-form').addClass('active');
	});

	$('#search-form .form-control').on('focusout', function () {
		$('#search-form').removeClass('active');
	});


	//js popup search box effect
	//----------------------------------
	$('.btn-search-form-toggle').on('click', function () {
		$("#popup-search-box-area").toggleClass('search-form-show');
	});
	//minicart activation jquery
	//====================
	$('#minicart').on('click', function () {
		$('.minicart-page-area').toggleClass('active');
	});

	/*-------------------------------------
	Single services
	---------------------------------------*/
	$(".preview a").on("click", function(){
		$(".selected").removeClass("selected");
		$(this).addClass("selected");
			var picture = $(this).data();
			event.preventDefault(); //prevents page from reloading every time you click a thumbnail
		$(".full img").fadeOut( 100, function() {
		$(".full img").attr("src", picture.full);
		$(".full").attr("href", picture.full);
		$(".full").attr("title", picture.title);

	}).fadeIn();
	});// end on click
	$(".full").fancybox({
		helpers : {
			title: {
				type: 'inside'
			}
		},
		closeBtn : true,
	});


	//isotops

	var $container = $('#isotop_sec').isotope({
		itemSelector: '.item',
		masonry: {
			columnWidth: '.grid-sizer',
			isFitWidth: false
		}
	});


	$("#seemore").on("click", function(){
		$(".quick_cat").show("slide", {direction:"left"}, 500);
		$("#closeseemore").css("display","block");
		$("#seemore").css("display","none");
	});
	$("#closeseemore").on("click", function(){
		$(".quick_cat").hide("slide", {direction:"left"}, 500);
		$("#seemore").css("display","block");
		$("#closeseemore").css("display","none");
	});

	$('#nav-icon1').on("click", function(){
		$(this).toggleClass('open');
		$(".quick_cat").toggle("slide", {direction:"left"}, 500);
	});


	$(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });

    $('.dpdn_menu').on("click", function(){
      $( this ).next().stop( true, true ).slideToggle();
    });

     $('.dropdown').on('show.bs.dropdown', function() {
       $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
     });

     $('.dropdown').on('hide.bs.dropdown', function() {
       $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });

    $("#owl-demo").owlCarousel({
        items: 1,
        lazyLoad: true,
        navigation: true,
        loop: true,
        autoPlay: 3000,
        navigation: false,
    });

});

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
