﻿app.controller("ParentCtrl", ["$rootScope", "$scope", "$state", "festiveMallLoginService", "festiveMallMasterService", "$timeout", "alertify", "Lightbox", function ($rootScope, $scope, $state, festiveMallLoginService, festiveMallMasterService, $timeout, alertify, Lightbox) {

    $scope.showAdminTab = false;
    $scope.showLoginBtn = true;
    $scope.showLogoutBtn = false;
    $scope.LoginUserName = "";
    $scope.EditedVarientData = "";
    $scope.varienttab = 1;
    $rootScope.homeCategoryId = null;
    $rootScope.homeCategoryName = 'All';
    var loginObj = sessionStorage.getItem("LoginObject");
    if (loginObj === null || loginObj === "" || loginObj === undefined) {
        $scope.headerActiveTab = 1;
    } else {
        loginObj = JSON.parse(loginObj);
        if (loginObj.Status === 1) {
            $scope.showAdminTab = true;
            $scope.showLoginBtn = false;
            $scope.showLogoutBtn = true;
            $scope.LoginUserName = loginObj.data.Firstname + " " + loginObj.data.Lastname;
        }
    }
    $scope.GoToHome = function (tabno) {
        $scope.headerActiveTab = tabno;
        $rootScope.homeCategoryId = null;
        $rootScope.homeCategoryName = 'All';
        $state.go("Home");
    };
    $scope.GoToAboutUs = function (tabno) {
        $scope.headerActiveTab = tabno;
        $state.go("AboutUs");
    };
    $scope.GoToContactUs = function (tabno) {
        $scope.headerActiveTab = tabno;
        $state.go("ContactUs");
    };
    $scope.GoToShop = function (tabno) {
        $scope.headerActiveTab = tabno;
        $rootScope.homeCategoryId = null;
        $rootScope.homeCategoryName = 'All';
        $state.go("Shop");
    };
    $scope.GoToAdmin = function (tabno) {
        $scope.headerActiveTab = tabno;
        $state.go("Admin");
    };
    $scope.showLoginPopup = false;
    $scope.GoToLogin = function () {
        $scope.showLoginPopup = true;
    };

    $scope.navtoShop = function (catId, catName) {
        console.log(catId);

        $scope.headerActiveTab = 4;

        $rootScope.homeCategoryId = catId;
        $rootScope.homeCategoryName = catName;

        $state.go("Shop");
    }


    $scope.validateLogin = function () {
        if ($scope.loginObject.userName === "") {
            alertify.error("Please Enter Valid UserName");
            return false;
        }
        if ($scope.loginObject.passWord === "") {
            alertify.error("Please Enter Password");
            return false;
        }
        var params = {
            "UserName": $scope.loginObject.userName,
            "Password": $scope.loginObject.passWord
        };
        festiveMallLoginService.ValidateUser($scope.loginObject.userName, $scope.loginObject.passWord, OnLoginSuccess, OnLoginError);
    };
    function OnLoginSuccess(resp) {
        if (resp.data !== undefined) {
            if (resp.data.Status === 1) {
                alertify.success("Login Successfully");
                sessionStorage.setItem("LoginObject", JSON.stringify(resp.data));
                $scope.LoginUserName = resp.data.data.Firstname + " " + resp.data.data.Lastname;
                $scope.loginObject = {
                    "userName": "",
                    "passWord": ""
                };
                $scope.showLoginPopup = false;
                $scope.showLoginBtn = false;
                $scope.showLogoutBtn = true;
                $scope.showAdminTab = true;
            }
        }
    }
    function OnLoginError(error) {
        alertify.error("Something Went Wrong");
    }
    $scope.NavToLogout = function () {
        alertify.success("Successfully Logged Out");
        sessionStorage.removeItem("LoginObject");
        $scope.GoToHome();
        $scope.showAdminTab = false;
        $scope.showLoginBtn = true;
        $scope.showLogoutBtn = false;
    };

    $scope.closeLoginPopup = function () {
        $scope.showLoginPopup = false;
        $scope.loginObject = {
            "userName": "",
            "passWord": ""
        };
    };
    $scope.loginObject = {
        "userName": "",
        "passWord": ""
    };

    $scope.MasterColors = [];
    $scope.MasterSizes = [];
    $scope.MasterCategories = [];
    $scope.showAddCategoryPopup = false;
    $scope.showAddProductPopup = false;
    $scope.showAddColorPopup = false;
    $scope.showAddSizePopup = false;
    $scope.categoryName = "";
    $scope.colorName = "";
    $scope.sizeName = "";
    $scope.productName = "";
    $scope.productDescription = "";
    $scope.price = "";
    $scope.discountAmount = "";

    $scope.selectedCategory = "";
    $scope.selectedSize = "";
    $scope.selectedColor = "";

    $scope.getColors = function () {
        festiveMallMasterService.GetColors(onsuccess, onerror);
        function onsuccess(resp) {
            $scope.MasterColors = resp.data;
            $scope.selectedColor = $scope.MasterColors[0];
            $scope.VarientObj.Varient.ColourId = $scope.selectedColor.ColourId;
        }
        function onerror(error) {
            console.log(error);
        }

    };
    $scope.ChangeColour = function (selectedColor) {

        $scope.VarientObj.Varient.ColourId = selectedColor.ColourId;

    };
    $scope.getSizes = function () {
        festiveMallMasterService.GetSizes(onsuccess, onerror);
        function onsuccess(resp) {
            $scope.MasterSizes = resp.data;
            $scope.selectedSize = $scope.MasterSizes[0];
            $scope.VarientObj.Varient.SizeId = $scope.selectedSize.SizeId;
        }
        function onerror(error) {
            console.log(error);
        }
    };
    $scope.ChangeSize = function (selectedSize) {

        $scope.VarientObj.Varient.SizeId = selectedSize.SizeId;

    };
    $scope.getCategories = function () {
        festiveMallMasterService.GetCategories(onsuccess, onerror);
        function onsuccess(resp) {
            $scope.MasterCategories = resp.data;
            $scope.selectedCategory = $scope.MasterCategories[0];
        }
        function onerror(error) {
            console.log(error);
        }
    };
    $scope.init = function () {
        $scope.getColors();
        $scope.getSizes();
        $scope.getCategories();
    };
    $scope.init();
    $scope.AddColor = function () {
        festiveMallMasterService.AddColor($scope.colorName, onsuccess, onerror);
        function onsuccess(resp) {

            if (resp.data !== undefined) {
                if (resp.data.Status === 1) {
                    $scope.closeAddColorPopup();
                    alertify.success("Color Added Successfully");
                    $scope.getColors();
                }
            }

        }
        function onerror(error) {
            alertify.error("Error Occured");
        }

    };
    $scope.AddSize = function () {
        festiveMallMasterService.AddSize($scope.sizeName, onsuccess, onerror);
        function onsuccess(resp) {

            if (resp.data !== undefined) {
                if (resp.data.Status === 1) {
                    $scope.closeAddSizePopup();
                    alertify.success("Size Added Successfully");
                    $scope.getSizes();
                }
            }
        }
        function onerror(error) {
            alertify.error("Error Occured");
        }

    };
    $scope.AddCategory = function () {
        festiveMallMasterService.AddCategory($scope.categoryName, onsuccess, onerror);
        function onsuccess(resp) {

            if (resp.data !== undefined) {
                if (resp.data.Status === 1) {
                    $scope.closeAddCategoryPopup();
                    alertify.success("Category Added Successfully");
                    $scope.getCategories();
                }
            }

        }
        function onerror(error) {
            alertify.error("Error Occured");
        }

    };
    $scope.clickToShowAddProduct = function () {

        if ($scope.selectedCategory === '') {
            $scope.showAddProductPopup = false;
            alertify.error("Select a Category");
        } else {
            if ($scope.selectedCategory.CategoryId === undefined) {
                alertify.error("Select a Category");
                $scope.showAddProductPopup = false;
            } else {
                $scope.showAddProductPopup = true;
            }
        }

    };
    $scope.showCategoryPopup = function () {
        $scope.showAddCategoryPopup = true;
    };
    $scope.showSizePopup = function () {
        $scope.showAddSizePopup = true;
    };
    $scope.showSizePopup = function () {
        $scope.showAddSizePopup = true;
    };
    $scope.showColorPopup = function () {
        $scope.showAddColorPopup = true;

    };

    $scope.ChangeCategory = function (selectedCategory) {

        $scope.selectedCategory = selectedCategory;


    };

    $scope.AddProduct = function () {
        var params = {
            "CategoryId": $scope.selectedCategory.CategoryId,
            "ProductId": 0,
            "ProductName": $scope.productName,
            "ProductDescription": $scope.productDescription
        };
        festiveMallMasterService.AddUpdateProduct(params, onsuccess, onerror);
        function onsuccess(resp) {
            if (resp.data !== undefined) {
                if (resp.data.Status === 1) {
                    $scope.closeAddProductPopup();
                    alertify.success("Product Added Successfully");
                }
            }
        }
        function onerror(error) {
            alertify.error("Error Occured");
        }
    };


    $scope.GetProducts = function (txt) {
        var object = {
            "CategoryId": $scope.selectedCategory.CategoryId,
            "SearchText": txt,
            "PageSize": 10,
            "PageNumber": 1
        };
        return festiveMallMasterService.GetProducts(object).then(function (response) {
            if (response.data !== undefined) {
                if (response.data.data.length > 0) {
                    return response.data.data.map(function (item) {
                        return item;
                    });
                }
            }
        });
    };
    $scope.closeAddCategoryPopup = function () {
        $scope.showAddCategoryPopup = false;
        $scope.categoryName = "";
    };
    $scope.closeAddColorPopup = function () {
        $scope.showAddColorPopup = false;
        $scope.colorName = "";
    };
    $scope.closeAddSizePopup = function () {
        $scope.showAddSizePopup = false;
        $scope.sizeName = "";
    };
    $scope.closeAddProductPopup = function () {
        $scope.showAddProductPopup = false;
        $scope.productName = "";
        $scope.productDescription = "";
    };
    $scope.upload = function (files, Image) {
        if (files.length === 0) {
            return;
        }
        var fileCount = $scope.FilesList.length + 1;
        Image.FileName = "0_" + fileCount + "." + files[0].name.substring(files[0].name.lastIndexOf('.') + 1);
        if ($scope.NameArray.indexOf(Image.FileName) === -1) {
            $scope.NameArray.push(Image.FileName);
        }
        files[0].VirtualFileName = Image.FileName;
        var existindex = _.findIndex($scope.FilesList, function (item) {
            return item.VirtualFileName === Image.FileName;
        });
        if (existindex > -1) {
            $scope.FilesList.splice(existindex, 1);
        }
        $scope.FilesList.push(files[0]);
        $timeout(function () {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(files[0]);
            fileReader.onload = function (e) {
                $timeout(function () {
                    Image.Url = e.target.result;
                });
            };
        }, 400);
    };
    $scope.RemovePicture = function (Image) {
        Image.IsDeleted = true;
        Image.IsModified = false;
        Image.Url = "";
        var index = _.findIndex($scope.FilesList, function (item) {
            return item.VirtualFileName === Image.FileName;
        });
        if (index > -1) {
            $scope.FilesList.splice(index, 1);
        }
        index = $scope.NameArray.indexOf(Image.FileName);
        if (index > -1) {
            $scope.NameArray.splice(index, 1);
        }
    };
    $scope.selectProduct = function (obj) {
        //$scope.productName = obj.ProductName;
        //$scope.productId = obj.productId;
        $scope.productName = obj.ProductName;
        $scope.VarientObj.Varient.ProductId = obj.ProductId;
    };





    $scope.FilesList = [];
    $scope.NameArray = [];
    $scope.GetVarientObj = function () {
        return {
            "Varient": {
                "ProductId": null,
                "ColourId": null,
                "SizeId": null,
                "UserId": null
            },
            "Stock": {
                "ProductVarientId": null,
                "Quantity": null,
                "Price": null,
                "MRP": null,
                "DiscountValue": null,
                "UserId": null,
                "ImageUrl": [
                    {
                        "StockImageDetailsId": null,
                        "Url": "",
                        "ActiveInd": "Y",
                        "FileName": null
                    },
                    {
                        "StockImageDetailsId": null,
                        "Url": "",
                        "ActiveInd": "Y",
                        "FileName": null
                    }, {
                        "StockImageDetailsId": null,
                        "Url": "",
                        "ActiveInd": "Y",
                        "FileName": null
                    },
                    {
                        "StockImageDetailsId": null,
                        "Url": "",
                        "ActiveInd": "Y",
                        "FileName": null
                    }
                ]
            }
        };
    };
    $scope.VarientObj = angular.copy($scope.GetVarientObj());
    $scope.ClearAddVarient = function () {
        $scope.VarientObj = angular.copy($scope.GetVarientObj());
    };
    $scope.AddVarient = function () {
        loginObj = sessionStorage.getItem("LoginObject");
        loginObj = JSON.parse(loginObj);
        $scope.VarientObj.Varient.UserId = loginObj.data.UserId;
        $scope.VarientObj.Stock.UserId = loginObj.data.UserId;
        festiveMallMasterService.PostServiceWithformData($scope.VarientObj, $scope.FilesList, $scope.NameArray).then(function (resp) {
            if (resp.data.Status !== 1) {
                alertify.error(resp.data.Message);
                return;
            } else {
                $scope.ClearAddVarient();
                $scope.varienttab = 3;
            }
        }, function (err) {
            console.log(err);
        });
    };
    $scope.VarientList = [];
    $scope.VarientCount = 0;
    $scope.maxsize = 5;
    $scope.VarientSearch = {
        "Searchtext": null,
        "PageSize": 10,
        "PageNo": 1,
        "CategoryId": null
    };
    $scope.changeVareintPage = function (PageNo) {
        $scope.GetVarientByProductId();
    };
    $scope.GetVarientByProductId = function () {
        festiveMallMasterService.GetVarientByProductId($scope.VarientSearch).then(function (resp) {
            if (resp.data.Status !== 1) {
                alertify.error(resp.data.Message);
                $scope.VarientList = [];
                $scope.VarientCount = 0;
                return;
            } else {
                $scope.VarientList = resp.data.data.rows;
                $scope.VarientCount = resp.data.data.count;
                angular.forEach($scope.VarientList, function (item, index) {
                    if (item.Images.length === 0) {
                        item.SelectedUrl = "";
                    } else {
                        item.SelectedUrl = item.Images[0].ImageUrl;
                    }
                });
            }
        }, function (err) {
            console.log(err);
            $scope.VarientList = [];
            $scope.VarientCount = 0;
        });
    };
    $scope.$watch("varienttab", function (newValue, oldaValue) {
        if (newValue === 3) {
            $scope.GetVarientByProductId();
        }
    });
    $scope.openLightboxModal = function (index, url) {
        $scope.ConsentFileUploadURlArray.push({
            "url": url,
            "thumbUrl": url
        });
        Lightbox.openModal($scope.ConsentFileUploadURlArray, index);
    };
    $scope.AllImagesArray = [];
    $scope.openImages = function (item) {
        $scope.AllImagesArray = [];
        angular.forEach(item.Images, function (subitem) {
            $scope.AllImagesArray.push({
                "url": subitem.ImageUrl,
                "thumbUrl": subitem.ImageUrl
            });

        });
        Lightbox.openModal($scope.AllImagesArray, 0);
    };
    $scope.DeleteVarient = function (obj) {
        alertify.confirm("<b class='txt-centre'>Confirm !</b><h4 class='txt-body-centre'>Want to delete?</h4> ", function () {
            festiveMallMasterService.DeleteVarient(obj).then(function (resp) {
                if (resp.data.Status !== 1) {
                    alertify.error(resp.data.Message);
                    return;
                } else {
                    alertify.success("Deleted Successfully");
                    //$scope.VarientSearch = {
                    //    "Searchtext": null,
                    //    "PageSize": 10,
                    //    "PageNo": 1,
                    //    "CategoryId": null
                    //};
                    $scope.GetVarientByProductId();
                }
            }, function (err) {
                console.log(err);
                alertify.success("Deleting Failed");
            });
        }, function () {
        });
    };
}]);