﻿var festiveMallServices = angular.module("festiveMallServices", []);
var header = { headers: { "Content-Type": "application/json" } };
var imageheader = { headers: { "Content-Type": "text/json", "Accept-Encoding": "gzip" } };

//var url = "http://localhost:1852/api";
var url = "https://festivemall-api.azurewebsites.net/api";


//(function getConfig() {
//    "use strict";
//    var xhr = new XMLHttpRequest();
//    xhr.open("get", "appconfig.json", window);
//    xhr.onload = function () {
//        var status = xhr.status;
//        if (status === 200) {
//            if (xhr.responseText) {
//                var response = JSON.parse(xhr.responseText);
//                window.url = response.urls.apiurl;
//            }
//        } else {
//            console.error("messages: Could not load confguration -> ERROR ->", status);
//        }
//    };
//    xhr.send();
//}
//)();

festiveMallServices.service('festiveMallLoginService', ['$http', '$q', function ($http, $q) {
    this.ValidateUser = function (username, password, callbackSuccess, callbackError) {
        var params = { "UserName": username, "Password": password };
        $http.post(url + "/Login/ValidateUserLogin", params, header).then(
            function (response) {
                callbackSuccess(response);
            },
            function (response) {
                callbackError(response);
            }
        );
    };
}]);
festiveMallServices.service('festiveMallMasterService', ['$http', '$q', function ($http, $q) {

    this.GetColors = function (callbackSuccess, callbackError) {
        $http.get(url + "/MasterData/GetColour", header).then(
            function (response) {
                callbackSuccess(response.data);
            },
            function (response) {
                callbackError(response.data);
            }
        );
    };

    this.GetSizes = function (callbackSuccess, callbackError) {
        $http.get(url + "/MasterData/GetSizes", header).then(
            function (response) {
                callbackSuccess(response.data);
            },
            function (response) {
                callbackError(response.data);
            }
        );
    };

    this.GetCategories = function (callbackSuccess, callbackError) {
        $http.get(url + "/MasterData/GetCategories", header).then(
            function (response) {
                callbackSuccess(response.data);
            },
            function (response) {
                callbackError(response.data);
            }
        );
    };

    this.AddSize = function (param, callbackSuccess, callbackError) {
        var params = { "SizeName": param };
        $http.post(url + "/MasterData/AddSize", params, header).then(
            function (response) {
                callbackSuccess(response);
            },
            function (response) {
                callbackError(response);
            }
        );
    };

    this.AddColor = function (param, callbackSuccess, callbackError) {
        var params = { "ColorName": param };
        $http.post(url + "/MasterData/AddColor", params, header).then(
            function (response) {
                callbackSuccess(response);
            },
            function (response) {
                callbackError(response);
            }
        );
    };

    this.AddCategory = function (param, callbackSuccess, callbackError) {
        var params = { "CategoryName": param };
        $http.post(url + "/MasterData/AddCategory", params, header).then(
            function (response) {
                callbackSuccess(response);
            },
            function (response) {
                callbackError(response);
            }
        );
    };

    this.GetProducts = function (params, callbackSuccess, callbackError) {

        return $http.get(url + "/MasterData/GetProducts?CategoryId=" + params.CategoryId + "&SearchText=" + params.SearchText + "&PageSize=" + params.PageSize + "&PageNumber=" + params.PageNumber, header);
        //  $http.get(url + "/MasterData/GetProducts?CategoryId=" + params.CategoryId + "&SearchText=" + params.SearchText + "&PageSize=" + params.PageSize + "&PageNumber=" + params.PageNumber , header).then(
        //    function (response) {
        //        callbackSuccess(response);
        //    },
        //    function (response) {
        //        callbackError(response);
        //    }
        //);
    };


    this.AddUpdateProduct = function (params, callbackSuccess, callbackError) {
        $http.post(url + "/MasterData/AddUpdateProduct", params, header).then(
            function (response) {
                callbackSuccess(response);
            },
            function (response) {
                callbackError(response);
            }
        );
    };

    this.PostServiceWithformData = function (postInfo, uploadedfile, newnamearray) {
        return $http({
            method: "POST",
            url: url + "/Admin/AddVarient",
            headers: { "Content-Type": undefined },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("model", angular.toJson(data.model));
                for (var i = 0; i < data.files.length; i++) {
                    if (newnamearray.length > 0) {
                        var newname = "";
                        newname = newnamearray[i];
                        formData.append("file" + i, data.files[i], newname);
                    } else {
                        formData.append("file" + i, data.files[i]);
                    }
                }
                return formData;
            },
            data: { model: postInfo, files: uploadedfile }
        });
    };

    this.GetVarientByProductId = function (obj) {
        return $http.post(url + "/Admin/GetVarientByProductId", obj);
    };
    this.Getstock = function (obj) {
        return $http.post(url + "/Admin/Getstock", obj);
    };
    this.DeleteVarient = function (obj) {
        return $http.post(url + "/Admin/DeleteVarient", obj);
    };
    this.SendMailToAdmin = function (obj) {
        return $http.post(url + "/MasterData/SendMailToAdmin", obj);
    };
}]);